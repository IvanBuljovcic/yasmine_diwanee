<?php 
	include 'site_modules/head.php';
	include 'site_modules/header.php';
?>

<!-- Main content -->
<div class="container">
	<!-- Full width image ad -->
	<section class="full-width bottom-margin-large relative fright no-padding">
		<div class="advertisment">
			<img src="assets/img/ads/header-1.jpg" alt="">
			<div class="overlay-text absolute">
				<h2>اليك اطلالات ايفا لونغوريا في حفل Global Gift Gala 2014</h2>
				<a href="javascript:;" class="categorie"> موضة‏ </a>
			</div>
		</div>
	</section>
	<!-- Articles -->
	<div class="wrapper">
		<div class="article-container half-width fright">
			<article>
				<div class="article-image">
					<img src="assets/img/photo-article1.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">موضة</a></span>
					<h2 class="article-title">
						مقابلة لياسمينة مع مصمّمة المجوهرات Cynthia Sakai
					</h2>
				</div>
			</article>
			<article>
				<div class="article-image">
					<img src="assets/img/photo-article2.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">أعراس‏</a></span>
					<h2 class="article-title">
						تصرفات العروس المزعجة في تحضيرات الزفاف
					</h2>
					<a href="javascript:;" class="brand">محتوى برعاية SEPHORA</a>
				</div>
			</article>
		</div>
		
		<!-- Left hand side widget -->
		<div class="widget">
			<article>
				<img src="assets/img/ads/small-ad1.jpg" alt="">
			</article>
		</div>

		<div class="article-container full-width fright">
			<article class="half-width">
				<div class="video-container relative">
					<img src="assets/img/video1.jpg" alt="">
					<span class="play"></span>
				</div>
				<div class="article-text article-text-wide full-width">
					<span class="categorie wide"><a href="javascript:;">موضة‏</a></span>
					<h2 class="article-title">شاهدي بالفيديو كيفيّة عمل مكياج زوي ديشانيل</h2>
					<a href="javascript:;" class="brand">محتوى برعاية SEPHORA</a>
				</div>
			</article>
			<article class="half-width last">
				<div class="article-image">
					<img src="assets/img/photo-article3.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">لايف ستايل</a></span>
					<h2 class="article-title">
						ما هي قواعد الاتيكيت التي تحدد لباقة المرأة؟
					</h2>
				</div>
			</article>
		</div>
	</div>		
	<!-- Full width image ad -->
	<section class="full-width background-dark">
		<img src="assets/img/ads/banner-full-2.jpg" alt="">
	</section>		
	<!-- Articles -->
	<div class="wrapper">
		<div class="article-container half-width fright">
			<article class="wide">
				<div class="article-image">
					<img src="assets/img/photo-article4.jpg" alt="">
				</div>
				<div class="article-text article-text-wide full-width">
					<span class="categorie wide"><a href="javascript:;">موضة‏</a></span>
					<h2 class="article-title">استعمالات جماليّة غير متوقّعة للقرفة</h2>
					<a href="javascript:;" class="brand">محتوى ياسمينة الحصري</a>
				</div>
			</article>
		</div>

		<div class="widget background-gray">
			<article>
				<h2 class="widget-title">Fashion Police</h2>
				<span class="separator hline"></span>
				<h3>إليسا تقع ضحيّة التكرار</h3>
				<img src="assets/img/widget2.jpg" alt="">
				<div class="read-more">
					<a href="javascript:;">إقرأي المزيد</a>
				</div>
			</article>
		</div>

		<div class="article-container full-width fright">
			<article class="half-width">
				<div class="article-image">
					<img src="assets/img/photo-article3.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">لايف ستايل</a></span>
					<h2 class="article-title">
						ما هي قواعد الاتيكيت التي تحدد لباقة المرأة؟
					</h2>
				</div>
			</article>
			<article class="half-width last border">
				<div class="article-image">
					<img src="assets/img/video2.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">موضة</a></span>
					<h2 class="article-title">سيفورا: ماكياج الديفا مع هدى قطان</h2>
				</div>
			</article>
		</div>
	</div>		
	<!-- Video section -->
	<section class="full-width background-gray">
		<div class="wrapper">
			<h2 class="section-title">أحدث الفيديوهات</h2>
			<div class="article-container half-width fright">
				<article>
					<div class="video-container">
						<img src="assets/img/video3.jpg" alt="">
						<span class="play"></span>
					</div>
					<div class="article-text article-text-wide full-width">
						<span class="categorie wide"><a href="javascript:;"> تجميل‏ </a></span>
						<h3 class="article-title">ميريام فارس تتألق في إعلان مكياجي</h3>
					</div>
				</article>
			</div>
			<div class="article-container half-width fright grid">
				<article class="grid-1-2">
					<div class="video-container">
						<img src="assets/img/video2.jpg" alt="">
						<span class="play"></span>
					</div>
					<div class="article-text">
						<span class="categorie"><a href="javascript:;"> تجميل‏ </a></span>
						<h3 class="article-title">شاهدي بالفيديو كيفيّة عمل مكياج زوي ديشانيل</h3>
					</div>
				</article>
				<article class="grid-1-2">
					<div class="video-container">
						<img src="assets/img/video4.jpg" alt="">
						<span class="play"></span>
					</div>
					<div class="article-text">
						<span class="categorie"><a href="javascript:;"> تجميل‏ </a></span>
						<h3 class="article-title">شاهدي بالفيديو كيفيّة عمل مكياج زوي ديشانيل</h3>
					</div>
				</article>
				<article class="grid-1-2">
					<div class="video-container">
						<img src="assets/img/video6.jpg" alt="">
						<span class="play"></span>
					</div>
					<div class="article-text">
						<span class="categorie"><a href="javascript:;"> تجميل‏ </a></span>
						<h3 class="article-title">شاهدي بالفيديو كيفيّة عمل مكياج زوي ديشانيل</h3>
					</div>
				</article>
				<article class="grid-1-2">
					<div class="video-container">
						<img src="assets/img/video5.jpg" alt="">
						<span class="play"></span>
					</div>
					<div class="article-text">
						<span class="categorie"><a href="javascript:;"> تجميل‏ </a></span>
						<h3 class="article-title">شاهدي بالفيديو كيفيّة عمل مكياج زوي ديشانيل</h3>
					</div>
				</article>
			</div>
			<!-- #Article grid -->
			<div class="read-more text-left">
				<a href="javascript:;">المزيد من  الفيديوهات</a>
			</div>
		</div>
	</section>
	<!-- Articles -->
	<div class="wrapper">
		<div class="article-container half-width fright">
			<article class="wide">
				<div class="article-image">
					<img src="assets/img/photo-article5.jpg" alt="">
				</div>
				<div class="article-text article-text-wide full-width">
					<span class="categorie wide"><a href="javascript:;">موضة‏</a></span>
					<h2 class="article-title">إيفا لونغوريا تتألق في دبيّ بأزياء المصمّمين العرب</h2>
				</div>
			</article>
			<article>
				<div class="article-image">
					<img src="assets/img/photo-article6.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"> <a href="javascript:;">من أنتِ‏</a> </span>
					<h2 class="article-title">الايرانية قونشيه قوامي تواجه مصيراً مجهولاً في السجن</h2>
				</div>
			</article>
			<article class="border">
				<div class="article-image">
					<img src="assets/img/photo-article7.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript">ماكياج</a></span>
					<h2 class="article-title">أسوأ خطوات ماكياج عليك التوّقف عنها</h2>
				</div>
			</article>
		</div>
		<div class="widget">
			<article>
				<h2 class="widget-title">GET THE LOOK</h2>
				<span class="separator hline"></span>
				<h3>إعتمدي ستايل الريف في صيف</h3>
				<img src="assets/img/widget3.jpg" alt="">
				<div class="read-more">
					<a href="javascript:;">إقرأي المزيد</a>
				</div>
			</article>
		</div>
		<div class="widget background-gray">
			<article>
				<img src="assets/img/widget4.jpg" alt="">
				<h2 class="widget-title">مقابلة حصرية</h2>
				<span class="separator hline"></span>
				<h3>مقابلة مميّزة لياسمينة مع Ivana Okereke</h3>
				<div class="read-more">
					<a href="javascript:;">إقرأي المزيد »</a>
				</div>
			</article>
		</div>
	</div>

	<!-- Photo section -->
	<section class="full-width background-gray">
		<div class="wrapper">
			<h2 class="section-title">أسبوع الموضة في باريس</h2>
			<div class="article-container half-width fright">
				<article class="last wide">
					<div class="article-image relative">
						<img src="assets/img/photo-article8.jpg" alt="">
						<span class="album"></span>
					</div>
					<div class="article-text article-text-wide full-width">
						<span class="categorie"><a href="javascript:;">موضة</a></span>
						<h3 class="article-title">ملخّص عن أبرز عروض أزياء اليوم الأوّل من فاشن فورورد</h3>
					</div>
				</article>
			</div>
			<div class="article-container gutter-width fright">
				<article>
					<div class="article-image relative">
						<img src="assets/img/photo-article9.jpg" alt="">
					</div>
					<div class="article-text half-width">
						<span class="categorie"><a href="javascript:;">تجميل</a></span>
						<h3 class="article-title">إليكِ عروض أزياء اليوم الأوّل من Fashion Forward</h3>
					</div>
				</article>
				<article>
					<div class="article-image relative">
						<img src="assets/img/photo-article10.jpg" alt="">
						<span class="album"></span>
					</div>
					<div class="article-text half-width">
						<span class="categorie"><a href="javascript:;">موضة</a></span>
						<h3 class="article-title">مقابلة مميّزة لياسمينة مع Arcadian Apparel</h3>
					</div>
				</article>
			</div>
			<div class="read-more text-left">
				<a href="javascript:;">إقرأي المزيد »</a>
			</div>
		</div>
	</section>

	<!-- Articles -->
	<div class="wrapper">
		<div class="article-container half-width fright">
			<article>
				<div class="article-image">
					<img src="assets/img/photo-article11.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">موضة</a></span>
					<h2 class="article-title">أيّة حقائب تختارين لصيف </h2>
				</div>
			</article>
			<article>
				<div class="article-image">
					<img src="assets/img/photo-article12.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">تجميل‏ </a></span>
					<h2 class="article-title">إلجأي إلى الفواكه للتخلّص من الهالات السوداء</h2>
				</div>
			</article>
			<article class="border">
				<div class="article-image">
					<img src="assets/img/photo-article13.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">ماكياج</a></span>
					<h2 class="article-title">إختاري البودرة بحسب هذه القواعد</h2>
				</div>
			</article>
		</div>
		<div class="widget background-gray">
			<article class="wide">
				<div class="widget-title-logo">
					<h2 class="widget-title">أخبار المشاهير من</h2>
					<img src="assets/img/ads/widget-logo.png" alt="">
				</div>
				<img src="assets/img/widget5.jpg" alt="">
				<div class="article-text article-text-wide full-width">
					<h3><span class="separator hline"></span>مايا دياب: "الإثارة هدية وصِفة من الله"</h3>
				</div>
			</article>
			<article class="text-right">
				<div class="article-image">
					<img src="assets/img/widget6.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="separator hline"></span>
					<h3 class="article-title">صور كيفانش تاتليتوغ يحتفل بعيد ميلاده مع حبيبته</h3>
				</div>
			</article>
			<article class="text-right">
				<div class="article-image">
					<img src="assets/img/widget7.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="separator hline"></span>
					<h3 class="article-title">صور كيفانش تاتليتوغ يحتفل بعيد ميلاده مع حبيبته</h3>
				</div>
			</article>
			<div class="read-more">
				<a href="javascript:;">إقرأي المزيد »</a>
			</div>
		</div>
		<div class="article-container large-article full-width fright">
			<article class="border">
				<div class="article-image half-width fright">
					<img src="assets/img/photo-article14.jpg" alt="">
				</div>
				<div class="article-text half-width fleft">
					<span class="categorie"><a href="javascript:;"> مواضيع‏</a></span>
					<h2 class="article-title">تجنّبي السكري وتلذذي بحلاوة الحياة</h2>
					<p>
						يصف الأطباء مرض السكري بأنه مجموعة من الأمراض الأيضية ينتج عنها ارتفاع نسبة الجلوكوز في الجسم أي نسبة السكر الموجودة في الدم، إن بسبب مشكلة في انتاج الجسم لمادة الأنسولين أو بسبب عدم استجابة الخلايا جيداً للأنسولين
					</p>
					<p>						
						ومن العوارض الأكثر شيوعاً والتي تنذر بمرض السكري نذكر: التبوّل المتكرر، العطش الشديد، الجوع الدائم، زيادة الوزن أو انخفاضه، التعب، غشاوة في الرؤية، عدم إلتحام الجروح بسهولة، إلتهابات جلدية، بشرة متهيّجة، إتهاب اللثة واحمرارهاغشاوة في الرؤية، عدم إلتحام الجروح بسهولة، إلتهابات جلدية، بشرة متهيّجة، إتهاب اللثة واحمرارها
					</p>
					<div class="read-more">
						<a href="javascript:;">إقرأي المزيد »</a>
					</div>
				</div>
			</article>
		</div>
		<div class="article-container half-width fright">
			<article>
				<div class="article-image">
					<img src="assets/img/photo-article15.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">صحتك‏ </a></span>
					<h2 class="article-title">
						ارشادات عامة لمصابات السكري
					</h2>
				</div>
			</article>
			<article class="border">
				<div class="article-image">
					<img src="assets/img/photo-article16.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">موضة</a></span>
					<h2 class="article-title">
						عندما تلتقي الموضة مع الهندسة تكون النتيجة فساتين جميلة
					</h2>
				</div>
			</article>
			<article class="border">
				<div class="article-image">
					<img src="assets/img/photo-article17.jpg" alt="">
				</div>
				<div class="article-text half-width">
					<span class="categorie"><a href="javascript:;">ماكياج</a></span>
					<h2 class="article-title">
						إليك اجمل مكياج وردي بالصور
					</h2>
				</div>
			</article>
		</div>
		<div class="widget" id="online-brands">
			<article>
				<h2 class="widget-title">
					ترند اون لاين
				</h2>
				<span class="separator hline"></span>
				<ul>
					<li>أجمل صور مكياج هندي لإطلالتك عام 2015</li>
					<li>شاهدي أحلى 5 فساتين زفاف 2015</li>
					<li>افضل 5 انواع كريم اساس من ماكMAC</li>
					<li>صور اجمل 5 فساتين</li>
					<li>أجمل صور مكياج هندي لإطلالتك عام 2015</li>
					<li>شاهدي أحلى 5 فساتين زفاف 2015</li>
					<li>افضل 5 انواع كريم اساس من ماكMAC</li>
					<li>صور اجمل 5 فساتين مايا دياب 2014</li>
				</ul>
				<div class="read-more">
					<a href="javascript:;">إقرأي المزيد »</a>
				</div>
			</article>
		</div>
	</div>

	<!-- Newsletter section -->
	<section class="full-width background-gray text-center white">
		<div class="wrapper">
			<h2 class="section-title">
				إبقي على إطلاع
			</h2>
			<span class="separator hline"></span>
			<p>الحصول على أحدث التقارير أخبار الموضة والاتجاهات، ويستقر على غرار الشارع، وتغطية المدرج، والحزب، وأكثر من على التوالي إلى صندوق البريد الوارد الخاص بك!</p>
			<div class="newsletter">
				<button class="submit">إشتركي</button>
				<input type="email" placeholder="أدخلي البريد الإلكتروني الخاص بك">
				<span class="email-icon"></span>
			</div>
		</div>
	</section>
</div>

<?php 
	include 'site_modules/footer.php';
?>