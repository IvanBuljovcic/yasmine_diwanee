<footer class="site-footer">
		<div class="wrapper">
			<h2 class="section-title">المزيد على ياسمينة</h2>
			<span class="separator hline"></span>
			<div class="article-container half-width fright brands-right">
				<div class="grid">
					<article class="grid-1-3">
						<img src="assets/img/footer1.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">Dior</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer2.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">H&amp;M</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer3.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">YSL</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer4.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">Aziz &amp; Walid Mouzannar</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer5.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">Agatha</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer6.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">Acne</span>
						</div>
					</article>
				</div>
				<div class="full-width brands-list">
					<ul class="clearfix">
						<li><a href="javascript:;">Govern</a></li>
						<li><a href="javascript:;">Regulations</a></li>
						<li><a href="javascript:;">Mass</a></li>
						<li><a href="javascript:;">Daunting</a></li>
						<li><a href="javascript:;">Despite</a></li>
						<li><a href="javascript:;">Companies</a></li>
						<li><a href="javascript:;">SucceededIndustry</a></li>
						<li><a href="javascript:;">Companies</a></li>
						<li><a href="javascript:;">Industry</a></li>
						<li><a href="javascript:;">Daunting</a></li>
					</ul>
					<span class="categorie"> جميع الماركات</span>
				</div>
			</div>
			<div class="article-container half-width fright brands-left">
				<div class="grid">
					<article class="grid-1-3">
						<img src="assets/img/footer7.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">ألين وطفاممثلة</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer8.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">ألين وطفاممثلة</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer9.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">ألين وطفاممثلة</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer10.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">ألين وطفاممثلة</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer11.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">ألين وطفاممثلة</span>
						</div>
					</article>
					<article class="grid-1-3">
						<img src="assets/img/footer12.jpg" alt="">
						<div class="article-text full-width fright">
							<span class="article-title">ألين وطفاممثلة</span>
						</div>
					</article>
				</div>
				<div class="full-width brands-list">
					<ul class="clearfix">
						<li><a href="javascript:;">إيلي صعب</a></li>
						<li><a href="javascript:;">باسيل سودا</a></li>
						<li><a href="javascript:;">بول سميث</a></li>
						<li><a href="javascript:;">بيار كترا</a></li>
						<li><a href="javascript:;">جورج شقرا</a></li>
						<li><a href="javascript:;">ديريك خان</a></li>
						<li><a href="javascript:;">ربيع كيروز</a></li>
						<li><a href="javascript:;">إيلي صعب</a></li>
						<li><a href="javascript:;">باسيل سودا</a></li>
						<li><a href="javascript:;">بول سميث</a></li>
						<li><a href="javascript:;">بيار كترا</a></li>
					</ul>
					<span class="categorie"> جميع الماركات</span>
				</div>
			</div>
			<div class="full-width fright topics">
				<span>مواضيع</span>
				<ul>
					<li><a href="javascript:;">أزياء النجمات</a></li>
					<li><a href="javascript:;">نصائح للجمال</a></li>
					<li><a href="javascript:;">أنواع فساتين</a></li>
					<li><a href="javascript:;">دروس مكياج</a></li>
					<li><a href="javascript:;">أنواع أزياء</a></li>
					<li><a href="javascript:;">صيحات الموضة</a></li>
					<li><a href="javascript:;">ترند اون لاين</a></li>
				</ul>
				<span class="categorie">جميع مواضيع</span>
			</div>
			<div class="copyright full-width fright">
				<div class="logo-container">
					<img src="assets/img/logo.png" alt="">
				</div>					
				<div class="licence">
					عن ياسمينة      -      إتّصل بنا     -       الخصوصية وشروط الإستخدام

				</div>
				<div class="social">
					<ul class="clearfix">
						<li><a href="" class="facebook"></a></li>
						<li><a href="" class="youtube"></a></li>
						<li><a href="" class="instagram"></a></li>
						<li><a href="" class="gplus">GPus</a></li>
						<li><a href="" class="twitter"></a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</body>
</html>