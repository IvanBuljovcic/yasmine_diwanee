<header class="site-header">
	<div class="banner">
		<img src="assets/img/ads/header-ad.jpg" alt="Sephora AD">
	</div>
	<div class="wrapper">
		<div class="text-center">
			<a href="javascript:;" class="logo">
				<img src="assets/img/logo.jpg" alt="Yasmina logo">
			</a>
		</div>
		<nav class="site-navigation desktop text-center">
			<ul class="text-center hide">
				<li>
					<a href="javascript:;" >موضة </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >تجميل </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >أعراس </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >ماكياج </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >لايف ستايل </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >من أنت </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >صحتك  </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >فيديوهات </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >إختبار الشخصية </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >سياحة وسفر </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >رشاقة ورجيم </a>
				</li>
				<span class="separator square"></span>
				<li>
					<a href="javascript:;" >حيث ييبض الجمال </a>
				</li>
			</ul>
		</nav>
		<div class="search-container">
			<span class="separator vline"></span>
			<span class="search"></span>
		</div>
		<nav class="site-navigation mobile text-center">
			
		</nav>
	</div>
</header>